package com.example.keanemizziassignment;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;

public class Home_Activity extends AppCompatActivity  {

    private DrawerLayout dl;
    private ActionBarDrawerToggle abdt;
    private NavigationView nav_view;
    BottomNavigationView nav_view_bottom;
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_);

        dl = (DrawerLayout)findViewById(R.id.dl);
        abdt = new ActionBarDrawerToggle(this,dl,R.string.open,R.string.close);
        abdt.setDrawerIndicatorEnabled(true);

        dl.addDrawerListener(abdt);
        abdt.syncState();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        nav_view = (NavigationView)findViewById(R.id.nav_view);
        nav_view_bottom = (BottomNavigationView)findViewById(R.id.nav_view_bottom);

        if(savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                    new JuiceFragment()).commit();
            nav_view.setCheckedItem(R.id.nav_juice);


        }

        //nav_view.setNavigationItemSelectedListener(this);


        nav_view.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int id = item.getItemId();

                switch(id)
                {
                    case R.id.nav_juice:
                        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                                new JuiceFragment()).commit();
                        nav_view_bottom.setSelectedItemId(R.id.nav_juice);
                        //Toast.makeText(Home_Activity.this, "Juice",Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.nav_presets:
                        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                                new PresetFragment()).commit();
                        nav_view_bottom.setSelectedItemId(R.id.nav_presets);
                        break;
                    case R.id.nav_profile:
                        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                                new ProfileFragment()).commit();
                        nav_view_bottom.setSelectedItemId(R.id.nav_profile);
                        break;
                    case R.id.nav_logout:
                        Toast.makeText(Home_Activity.this, "Successfully logged out",Toast.LENGTH_SHORT).show();
                        mAuth.getInstance().signOut();
                        Intent intent = new Intent(Home_Activity.this,MainActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);

                        break;
                    default:
                        return true;
                }
                dl.closeDrawer(GravityCompat.START);
                return true;
            }

        });

        nav_view_bottom.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int id = item.getItemId();
                switch (id) {

                    case R.id.nav_juice:
                        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                                new JuiceFragment()).commit();
                        nav_view.setCheckedItem(R.id.nav_juice);
                        //Toast.makeText(Home_Activity.this, "Juice",Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.nav_presets:
                        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                                new PresetFragment()).commit();
                        nav_view.setCheckedItem(R.id.nav_presets);
                        break;
                    case R.id.nav_profile:
                        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                                new ProfileFragment()).commit();
                        nav_view.setCheckedItem(R.id.nav_profile);
                        break;
                }
                return true;
            }
        });



    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if(abdt.onOptionsItemSelected(item))
            return true;

        return super.onOptionsItemSelected(item);
    }
}
