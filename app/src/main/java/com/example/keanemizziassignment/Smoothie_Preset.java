package com.example.keanemizziassignment;

public class Smoothie_Preset {
    private String Smoothie;
    private String Description;
    private String Calories;
    private int Thumbnail;

    public Smoothie_Preset(){

    }
    //Smoothie Constructor
    public Smoothie_Preset(String smoothie, String description, String calories, int thumbnail) {
        Smoothie = smoothie;
        Description = description;
        Calories = calories;
        Thumbnail = thumbnail;
    }

    public String getSmoothie() {
        return Smoothie;
    }

    public String getDescription() {
        return Description;
    }

    public String getCalories() {
        return Calories;
    }

    public int getThumbnail() {
        return Thumbnail;
    }

    public void setSmoothie(String smoothie) {
        Smoothie = smoothie;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public void setCalories(String calories) {
        Calories = calories;
    }

    public void setThumbnail(int thumbnail) {
        Thumbnail = thumbnail;
    }
}
