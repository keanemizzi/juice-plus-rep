package com.example.keanemizziassignment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.keanemizziassignment.R;

import java.util.concurrent.ExecutionException;

public class JuiceFragment extends Fragment {

    EditText input1,input2;
    TextView txtview;
    Button btn;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_juice,container,false);

        //First Text box
        //Second Text Box
        //input1 = (EditText) getView().findViewById(R.id.input1);
        //input2 = (EditText) getView().findViewById(R.id.input2);

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        btn = (Button)getView().findViewById(R.id.btn);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getJuice(v);
            }
        });
    }

    public void getJuice(View v){


        input1 = (EditText) getView().findViewById(R.id.input1);
        input2 = (EditText) getView().findViewById(R.id.input2);



        GetJuiceTask task = new GetJuiceTask();

        try {
            double juice = task.execute(input1.getText().toString(),input2.getText().toString()).get();
            //double juice = task.execute(input1.getText().toString()).get();

            txtview = getView().findViewById(R.id.tv_calories);

        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }




}
