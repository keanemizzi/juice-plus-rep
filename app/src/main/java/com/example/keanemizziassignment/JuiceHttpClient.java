package com.example.keanemizziassignment;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;


public class JuiceHttpClient {

    private static String BASE_URL = "https://api.edamam.com/api/nutrition-data?app_id=65e434e5&app_key=71313934ac3da54389d776acea2a6338";
    private static String Ingredient ="&ingr=";
    private static String URLEnocde= "%20";

    public String getJuiceData(String quantity, String item){
        HttpURLConnection con = null ;
        InputStream is = null;
        try {
            String url = BASE_URL + Ingredient + quantity + URLEnocde + item;

            con = (HttpURLConnection) ( new URL(url)).openConnection();
            con.setRequestMethod("GET");
            con.setDoInput(true);
            con.setDoOutput(true);
            con.connect();

            StringBuffer buffer = new StringBuffer();
            is = con.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String line = null;
            while (  (line = br.readLine()) != null )
                buffer.append(line + "\r\n");

            is.close();
            con.disconnect();

            return buffer.toString();
        }
        catch(Throwable t) {
            t.printStackTrace();
        }
        finally {
            try { is.close(); } catch(Throwable t) {}
            try { con.disconnect(); } catch(Throwable t) {}
        }

        return null;
    }
}
