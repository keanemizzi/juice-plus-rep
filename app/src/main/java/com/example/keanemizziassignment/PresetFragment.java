package com.example.keanemizziassignment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

public class PresetFragment extends Fragment {

    List<Smoothie_Preset> listSmoothie;
    Context context;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {


        return inflater.inflate(R.layout.fragment_presets,container,false);


    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


        listSmoothie = new ArrayList<>(); //10 TOTAL SMOOTHIES
        listSmoothie.add(new Smoothie_Preset("Strawberry Smoothie","Smoothie Description....",
                "200 calories",R.drawable.smoothie_strawberry));
        listSmoothie.add(new Smoothie_Preset("Mango Smoothie","Smoothie Description....",
                "200 calories",R.drawable.smoothie_mango));
        listSmoothie.add(new Smoothie_Preset("Orange Smoothie","Smoothie Description....",
                "200 calories",R.drawable.smoothie_orange));
        listSmoothie.add(new Smoothie_Preset("Detox Smoothie","Smoothie Description....",
                "200 calories",R.drawable.smoothie_green));
        listSmoothie.add(new Smoothie_Preset("Mixed Berry Smoothie","Smoothie Description....",
                "200 calories",R.drawable.smoothie_berry));
        listSmoothie.add(new Smoothie_Preset("Strawberry Smoothie","Smoothie Description....",
                "200 calories",R.drawable.smoothie_strawberry));
        listSmoothie.add(new Smoothie_Preset("Mango Smoothie","Smoothie Description....",
                "200 calories",R.drawable.smoothie_mango));
        listSmoothie.add(new Smoothie_Preset("Orange Smoothie","Smoothie Description....",
                "200 calories",R.drawable.smoothie_orange));
        listSmoothie.add(new Smoothie_Preset("Detox Smoothie","Smoothie Description....",
                "200 calories",R.drawable.smoothie_green));
        listSmoothie.add(new Smoothie_Preset("Mixed Berry Smoothie","Smoothie Description....",
                "200 calories",R.drawable.smoothie_berry));
        listSmoothie.add(new Smoothie_Preset("Strawberry Smoothie","Smoothie Description....",
                "200 calories",R.drawable.smoothie_strawberry));
        listSmoothie.add(new Smoothie_Preset("Mango Smoothie","Smoothie Description....",
                "200 calories",R.drawable.smoothie_mango));
        listSmoothie.add(new Smoothie_Preset("Strawberry Smoothie","Smoothie Description....",
                "200 calories",R.drawable.smoothie_strawberry));
        listSmoothie.add(new Smoothie_Preset("Mango Smoothie","Smoothie Description....",
                "200 calories",R.drawable.smoothie_mango));
        listSmoothie.add(new Smoothie_Preset("Orange Smoothie","Smoothie Description....",
                "200 calories",R.drawable.smoothie_orange));
        listSmoothie.add(new Smoothie_Preset("Detox Smoothie","Smoothie Description....",
                "200 calories",R.drawable.smoothie_green));
        listSmoothie.add(new Smoothie_Preset("Mixed Berry Smoothie","Smoothie Description....",
                "200 calories",R.drawable.smoothie_berry));
        listSmoothie.add(new Smoothie_Preset("Strawberry Smoothie","Smoothie Description....",
                "200 calories",R.drawable.smoothie_strawberry));


        context = getContext();
        RecyclerView myrv = (RecyclerView)getView().findViewById(R.id.recycler_view);
        RecyclerViewAdapter myAdapter = new RecyclerViewAdapter(context,listSmoothie);
        myrv.setLayoutManager(new GridLayoutManager(context,3));
        myrv.setAdapter(myAdapter);
    }
}
