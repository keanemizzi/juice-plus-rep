package com.example.keanemizziassignment;

import android.os.AsyncTask;

import com.firebase.ui.auth.data.model.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class GetJuiceTask extends AsyncTask<String,Void,Double>{
    //private static String BASE_URL = "https://api.edamam.com/api/nutrition-data?app_id=65e434e5&app_key=71313934ac3da54389d776acea2a6338";

    @Override
    protected Double doInBackground(String... strings) {

        double calories = 0;
        JuiceHttpClient client = new JuiceHttpClient();

        String resultJSON = client.getJuiceData(strings[0],strings[0]);
        JSONObject jObj = null;

        try {
            jObj = new JSONObject(resultJSON);

            JSONObject mainObj = jObj.getJSONObject("main");
            calories = mainObj.getDouble("calories");

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return calories;
    }



}

