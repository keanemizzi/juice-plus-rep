package com.example.keanemizziassignment;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseUser;

public class SignUp_Activity extends AppCompatActivity implements View.OnClickListener{
    ProgressBar progressBar;
    AutoCompleteTextView TextEmail_Signup, TextPassword_Signup, getEditTextPasswordConfirm;
    private FirebaseAuth mAuth;
    Button signUp;

    public static final String SHARED_PREFS_Signup = "sharedPrefs";
    public static final String PrefsUsername_Signup = "sharedPrefs";
    private String emailPrefsSignup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        signUp = findViewById(R.id.sign_up_button);
        signUp.setOnClickListener(this);

        findViewById(R.id.loginScreen).setOnClickListener(this);
        findViewById(R.id.loginScreen_arrow).setOnClickListener(this);
        //findViewById(R.id.b)

        TextEmail_Signup = findViewById(R.id.editTextEmail_Signup);
        TextPassword_Signup = findViewById(R.id.editTextPassword);
        getEditTextPasswordConfirm = findViewById(R.id.editTextPasswordConfirm);
        mAuth = FirebaseAuth.getInstance();
        progressBar = (ProgressBar) findViewById(R.id.progressbar);


    }

    @Override
    protected void onStart() {
        super.onStart();
        loadData2();
        updateViews2();
    }

    @Override
    protected void onResume() {
        super.onResume();
        loadData2();
        updateViews2();
    }

    @Override
    protected void onPause() {
        super.onPause();
        saveData2();
    }

    @Override
    protected void onStop() {
        super.onStop();
        saveData2();
    }



    private void registerUser() {
        String email = TextEmail_Signup.getText().toString().trim();
        String password = TextPassword_Signup.getText().toString().trim();
        String passwordconfirm = getEditTextPasswordConfirm.getText().toString().trim();

        if(email.isEmpty()){
            TextEmail_Signup.setError("Email is Required");
            TextEmail_Signup.requestFocus();
            return;
        }
        if(!Patterns.EMAIL_ADDRESS.matcher(email).matches()){
            TextEmail_Signup.setError("Please enter a valid email");
            TextEmail_Signup.requestFocus();
            return;
        }

        if(password.isEmpty()){
            TextPassword_Signup.setError("Password is Required");
            TextPassword_Signup.requestFocus();
            return;
        }
        if(password.length() < 8 ){
            TextPassword_Signup.setError("Password needs to be a minimum of 8 characters");
            TextPassword_Signup.requestFocus();
            return;
        }
        if(!password.equals(passwordconfirm)){
            TextPassword_Signup.setError("Passwords do not match");
            TextPassword_Signup.requestFocus();
            return;
        }
        progressBar.setVisibility(View.VISIBLE);

        mAuth.createUserWithEmailAndPassword(email,password).addOnCompleteListener
                (new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                progressBar.setVisibility(View.GONE);
                if(task.isSuccessful()){
                    Toast.makeText(getApplicationContext(),"User Registered Successfully",
                            Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(SignUp_Activity.this,Home_Activity.class));
                        Intent intent = new Intent();
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);

                }else if(task.getException() instanceof FirebaseAuthUserCollisionException){
                    Toast.makeText(getApplicationContext(),"Email is already registered",
                            Toast.LENGTH_SHORT).show();//Checking if the email is already registered
                } else{
                    Toast.makeText(getApplicationContext(),"Error encountered..",
                            Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void saveData2(){
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS_Signup, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(PrefsUsername_Signup, TextEmail_Signup.getText().toString());
        editor.apply();

    }
    public void loadData2(){
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS_Signup, MODE_PRIVATE);
        emailPrefsSignup = sharedPreferences.getString(PrefsUsername_Signup,"");
    }
    public void updateViews2(){
        TextEmail_Signup.setText(emailPrefsSignup);
    }


    @Override
    public void onClick(View v) {
        switch(v.getId()){

            case R.id.sign_up_button:
                registerUser();
                signUp.onEditorAction(EditorInfo.IME_ACTION_DONE);
                break;

            case R.id.loginScreen:
                startActivity(new Intent(this,MainActivity.class));
                break;

            case R.id.loginScreen_arrow:
                startActivity(new Intent(this,MainActivity.class));
                break;
        }
    }


}
