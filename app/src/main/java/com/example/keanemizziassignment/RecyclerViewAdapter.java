package com.example.keanemizziassignment;

import android.content.Context;
import android.content.Intent;
import android.media.Image;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.List;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.MyViewHolder> {

    private Context mContext;
    private List<Smoothie_Preset> mData;

    public RecyclerViewAdapter(Context mContext, List<Smoothie_Preset> mData) {
        this.mContext = mContext;
        this.mData = mData;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        LayoutInflater mInflater = LayoutInflater.from(mContext);
        view = mInflater.inflate(R.layout.cardview_items,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, final int i) {
        myViewHolder.smoothie_title.setText(mData.get(i).getSmoothie());
        myViewHolder.img_smoothie_thumbnail.setImageResource(mData.get(i).getThumbnail());

        myViewHolder.cardview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*
                Intent intent = new Intent(mContext,SmoothieCVfragment.class);
                intent.putExtra("smoothie", mData.get(i).getSmoothie());
                intent.putExtra("desription", mData.get(i).getDescription());
                intent.putExtra("calories", mData.get(i).getCalories());
                intent.putExtra("Thumbnail", mData.get(i).getThumbnail());

                //getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                   //     new ProfileFragment()).commit();
                //mContext.
                */
            }
        });

        //set click listener

    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder{

        TextView smoothie_title;
        ImageView img_smoothie_thumbnail;

        CardView cardview;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            smoothie_title = (TextView) itemView.findViewById(R.id.smoothie_name_id);
            img_smoothie_thumbnail = (ImageView) itemView.findViewById(R.id.smoothie_image_id);
            cardview = (CardView) itemView.findViewById(R.id.cardview_id);
        }
    }

}
